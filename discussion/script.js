// INTRODUCTION TO JSON
// JS Object Notation
	// is a data format used by applications to store and transport data to one another
	// files that are saved with a file extension of .json

/*
MINIACTIVITY
	createa a JS object with the following properties:
		-city
		-province
		-country
*/

// DIFFERENCE
/*
1. Quotation Marks
	JS Objects only has the value inserted inside quotation marks.
	JSON has the quotation marks for both key and the value.
2. 
	JS objects - exclusive to javascript
	JSON - not exclusive for javascript. Other programming languages can also use JSON files
*/


/*const cities = {
	city: "Marikina",
	province: "NCR",
	country: "PH"
};

console.log(cities);*/


// JSON Object
/*
	JavaScript Object Notation
	JSON - used to serializing/deserializing different datat types into byte
		Serialization - process of converting data into series of bytes for easier transmission or transfer of information
*/
/*const cities = {
	"city": "Marikina",
	"province": "NCR",
	"country": "PH"
};

console.log(cities);*/



// JSON Arrays
/*let cities = [
{
	"city": "Ormoc",
	"province": "Leyte",
	"country": "PH"
},
{
	"city": "Bacoor",
	"province": "Cavite",
	"country": "PH"
},
{
	"city": "New York",
	"province": "Britain",
	"country": "PH"
}
];

console.log(cities);*/


// JSON METHODS
	// JSON object contains method for parsing and converting data into stringified JSON
	// Stringified JSON - JSON Object converted into string to be used in other functions of the language esp. javascript-based applications (serialize)

let batches = [
{
	"batchName": "Batch X"
},
{
	"batchName": "Batch Y"
}
];

console.log("Result from console.log");
console.log(batches);

console.log("Result from stringify method");
// stringify - used to convert JSON Objects into JSON (string)
console.log(JSON.stringify(batches));

let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "Manila",
		country: "America"
	}
});

console.log(data);

/*
ASSIGNMENT
	create userDetails variable that will contain js object with the following properties:
		fname - prompt
		lname - prompt
		age - prompt
		address: {
			city - prompt
			country - prompt
			zipCode - prompt
		};
log in the console the converted JSON data type
*/

/*let userDetails = JSON.stringify({
	fname: prompt("First Name"),
	lname: prompt("Last Name"),
	age: prompt("Age"),
	address: {
		city: prompt("City"),
		country: prompt("Country"),
		zipCode: prompt("Zip Code")
	}
});

console.log(userDetails);*/


// CONVERTING OF STRINGIFIED JSON INTO JS OBJECTS
	// PARSE Method - converting json data into js objects
	// information is commonly sent to application in STRINGIFIED JSON; then converted into objects; this happens both for sending information to a backend app such as the databases and back to frontend app such as the webpages
	// upon receiving the data, JSON text can be converted into a JS/JSON Object with parse method
let batchesJSON = `[
{
	"batchName": "Batch X"
},
{
	"batchName": "Batch Y"
}
]`;
console.log(batchesJSON);
console.log("Result from parse method:");
console.log(JSON.parse(batchesJSON));



let stringifiedData = `{
	"name": "John",
	"age": "31",
	"address": {
		"city": "Manila",
		"country": "Philippines"
	}
}`;

// how do we convert the variable above into a JS Object and log it in our console?

console.log(JSON.parse(stringifiedData));